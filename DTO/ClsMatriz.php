<?php

class ClsMatriz {

    private $matriz;
    private $tamano;

    public function __construct() {
        
    }
    /**
     * Construye la matriz inicial
     */
    public function ConstruirMatriz() {
        $this->matriz = array();

        for ($i = 0; $i <= $this->tamano; $i++) {
            $arrayX = array();
            for ($j = 0; $j <= $this->tamano; $j++) {
                $arrayY = array();
                for ($k = 0; $k <= $this->tamano; $k++) {
                    $arrayY[$k] = 0;
                }
                array_push($arrayX, $arrayY);
            }
            array_push($this->matriz, $arrayX);
        }
    }

    function getTamano() {
        return $this->tamano;
    }

    function setTamano($tamano) {
        $this->tamano = $tamano;
    }

    function getMatriz() {
        return $this->matriz;
    }

    function setMatriz($matriz) {
        $this->matriz = $matriz;
    }

}
