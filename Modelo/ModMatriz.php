<?php

class ModMatriz {

    public function __construct() {
        
    }

    /**
     * Actualiza la matriz dependiendo una coordenada
     * @param cordenadas: x,y,z donde se quiere actualizar
     *        dto: matriz
     * @return actualizacion exitosa o no
     */
    public function Actualizar($cordenadas, $dto) {
        $splitCordenada = explode("-", $cordenadas);
        $x = $splitCordenada[0];
        $y = $splitCordenada[1];
        $z = $splitCordenada[2];

        $matrizAuxiliar = $dto;
        $matrizAuxiliar[$x][$y][$z] = $splitCordenada[3];
        $_SESSION['matriz'] = serialize($matrizAuxiliar);

        $respuesta = "Actualizacion Completa" . "+" . "exito";

        return $respuesta;
    }
    /**
     * realiza la suma del rango ingresado
     * @param cordenadas: x1,y1,1,x2.y2,z2 rango de la suma
     *        dto: matriz
     * @return la suma del rango ingresaso
     */

    public function Consultar($cordenadas, $dto) {
        $splitCordenada = explode("-", $cordenadas);
        $x1 = $splitCordenada[0];
        $y1 = $splitCordenada[1];
        $z1 = $splitCordenada[2];
        $x2 = $splitCordenada[3];
        $y2 = $splitCordenada[4];
        $z2 = $splitCordenada[5];

        $sum = 0;

        for ($i = $x1; $i <= $x2; $i++) {
            for ($j = $y1; $j <= $y2; $j++) {
                for ($k = $z1; $k <= $z2; $k++) {
                    $sum+=$dto[$i][$j][$k];
                }
            }
        }
        $respuesta = "suma: " . $sum;
        return $respuesta;
    }

}
