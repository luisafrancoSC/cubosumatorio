<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="Scripts/bootstrap-3.3.6-dist/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="Recursos/CssGeneral.css">
        <script type="text/javascript" src="Scripts/JQuery.js"></script>
        <script type="text/javascript" src="Scripts/bootstrap-3.3.6-dist/js/bootstrap.js"></script>
        <script type="text/javascript" src="Scripts/Componentes.js"></script>
        <title>Cubo Sumatorio</title>
    </head>

    <body>
        <div class="contenedor">


            <div class="titulo">
                <h1 class="white-text"><b>Cubo Sumatorio</b></h1>
            </div>
            <br>
            <div id="alertas"></div>
            <div class="form-group col-lg-7" >
                <label for="numerodecasos">Numero de casos de prueba</label>
                <input type="text" class="form-control" id="numerodecasos" placeholder="Numero de casos">
            </div>
            <div class="form-group col-lg-7" >
                <label for="dimensionmatriz">Dimension de la matriz</label>
                <input type="text" class="form-control" id="dimensionmatriz" placeholder="Dimension de la mtriz">
            </div>
            <div class="form-group col-lg-7" >
                <label for="numerosentencias">Cantidad de sentencias</label>
                <input type="text" class="form-control" id="numerosentencias" placeholder="cantidad de sentencias">
            </div>
            <div class="form-group col-lg-7" >
                <button type="button" class="btn btn-primary" onclick="Agregar()" id="BtnAgregar">Agregar</button>
            </div>

            <div class="form-group col-lg-7" >
                <label for="Tiposentencia">Tipo de sentencia</label>
                <select class="form-control" id="select" onclick="select()">
                    <option value="Actualización">Actualización</option>
                    <option value="Consulta">Consulta</option>
                </select>
            </div>
            <div class="form-group col-lg-12" > 
                <div class="sentencias" id="sentencias">         

                </div>
            </div>
            <div class="form-group col-lg-7" >
                <button type="button" id="BtnAgregarSentencias" class="btn btn-success" onclick="AgregarSentencias()">Agregar Sentencia</button>
            </div>
            <div class="form-group col-lg-7" >
                <textarea class="form-control" rows="3" id="textArea" readonly="true"></textarea>
                <br>
                <button type="button" class="btn btn-primary" onclick="Recargarpg()">Reiniciar</button>
            </div>
        </div>

        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Soluciòn</h4>
                    </div>
                    <div class="modal-body" id="repuesta">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    </body>
</html>
