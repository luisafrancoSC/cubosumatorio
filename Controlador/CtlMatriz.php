<?php

session_start();
$accion = filter_input(INPUT_POST, 'accion');


switch ($accion) {
    case 'crear':
        crearMatriz();
        break;
    case 'calcular':
        calcular();
        break;
}
/**
 * llama al crea de la matriz tridimencional 
 */
function crearMatriz() {
    $dimension = filter_input(INPUT_POST, 'dimensionmatriz');

    require_once '../DTO/ClsMatriz.php';
    $dtoMatriz = new ClsMatriz();
    $dtoMatriz->setTamano($dimension);
    $dtoMatriz->ConstruirMatriz();
    $_SESSION["matriz"] = serialize($dtoMatriz->getMatriz());
    
    calcular();
}
/**
 * llama a la operacion ya sea actualizar o consultar
 * @return la repuesta ya sea que actualizo correctamente o no.
 *         la solucion de la consulta
 */
function calcular() {
    $sentencia = filter_input(INPUT_POST, 'sentencia');
    $cordenadas = filter_input(INPUT_POST, 'cordenadas');
    
    require_once '../DTO/ClsMatriz.php';
    $dtoMatriz = unserialize($_SESSION["matriz"]);

    require_once '../Modelo/ModMatriz.php';
    $modelo = new ModMatriz();
    if ($sentencia == "Actualización") {
        $respuesta = $modelo->Actualizar($cordenadas, $dtoMatriz);
    } else {
        $respuesta = $modelo->Consultar($cordenadas, $dtoMatriz);
    }

    echo $respuesta;
}
