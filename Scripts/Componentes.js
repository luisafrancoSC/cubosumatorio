$(window).load(function () {
    $("#select").attr('disabled', 'disabled');
    $("#BtnAgregarSentencias").attr('disabled', 'disabled');
});

var respuesta = "";
var cantidadcasos = 0;
var cantidadsentencias = 0;

/**
 * Agrega la configuración del numero de casos, dimension de la matriz y numero de sentencias a ejecutar 
 * verificando que todas la configuración sea correcta.
 */
function Agregar() {
    if (cantidadcasos < parseInt($("#numerodecasos").val())) {
        var numeroCasos = parseInt($("#numerodecasos").val());
        var dimensionMatriz = parseInt($("#dimensionmatriz").val());
        var numeroSentencias = parseInt($("#numerosentencias").val());
        if (numeroCasos >= 1 && numeroCasos <= 50) {
            if (dimensionMatriz >= 1 && dimensionMatriz <= 100) {
                if (numeroSentencias >= 1 && numeroSentencias <= 1000) {
                    $("#select").removeAttr('disabled', 'disabled');
                    $("#BtnAgregarSentencias").removeAttr('disabled', 'disabled');

                    $("#numerodecasos").attr('disabled', 'disabled');
                    $("#numerosentencias").attr('disabled', 'disabled');
                    $("#dimensionmatriz").attr('disabled', 'disabled');
                    $("#BtnAgregar").attr('disabled', 'disabled');
                    if (cantidadcasos === 0)
                        $("#textArea").val($("#textArea").val() + "\n" + numeroCasos);
                    cantidadcasos++;

                    $("#textArea").val($("#textArea").val() + "\n" + dimensionMatriz + " " + numeroSentencias);
                    $("#sentencias").html('<div class="form-group col-lg-1" ><label for="x">X</label><input type="text" class="form-control" id="x" placeholder="x"> </div> <div class="form-group col-lg-1" ><label for="y">Y</label><input type="text" class="form-control" id="y" placeholder="Y"> </div>  <div class="form-group col-lg-1" ><label for="z">Z</label><input type="text" class="form-control" id="z" placeholder="Z"> </div><div class="form-group col-lg-1" ><label for="w">W</label><input type="text" class="form-control" id="w" placeholder="W"> </div>');
                    Mensaje("exito", "Configure el primer caso de prueba");
                } else {
                    Mensaje("error", "El numero de sentencias debe ser mayor o igual que 1 y menor o igual  que 1000");
                }
            } else {
                Mensaje("error", "La dimension de la matriz debe ser mayor o igual  que 1 y menor o igual que 100");
            }
        } else {
            Mensaje("error", "El numero de casos de test debe ser mayor o igual que 1 y menor o igual que 50");
        }
    }
}
/**
 * Verifica cuando el select de las sentencias ha cambiado y si es asi inyecta el codigo html de los parametros
 *
 */
function select() {
    var valor = $("#select").val();
    if (valor === "Actualización") {
        $("#sentencias").html('<div class="form-group col-lg-1" ><label for="x">X</label><input type="text" class="form-control" id="x" placeholder="x"> </div> <div class="form-group col-lg-1" ><label for="y">Y</label><input type="text" class="form-control" id="y" placeholder="Y"> </div>  <div class="form-group col-lg-1" ><label for="z">Z</label><input type="text" class="form-control" id="z" placeholder="Z"> </div><div class="form-group col-lg-1" ><label for="w">W</label><input type="text" class="form-control" id="w" placeholder="W"> </div>');
    }
    else {
        $("#sentencias").html('<div class="form-group col-lg-1" ><label for="x1">X1</label><input type="text" class="form-control" id="x1" placeholder="x1"> </div> <div class="form-group col-lg-1" ><label for="y1">Y1</label><input type="text" class="form-control" id="y1" placeholder="Y1"> </div>  <div class="form-group col-lg-1" ><label for="z1">Z1</label><input type="text" class="form-control" id="z1" placeholder="Z1"> </div> <div class="form-group col-lg-1" ><label for="x2">X2</label><input type="text" class="form-control" id="x2" placeholder="x2"> </div> <div class="form-group col-lg-1" ><label for="y2">Y2</label><input type="text" class="form-control" id="y2" placeholder="Y2"> </div>  <div class="form-group col-lg-1" ><label for="z2">Z2</label><input type="text" class="form-control" id="z2" placeholder="Z2">');
    }
}
/**
 * Agregar las sentencias al textarea, verificando que todos los parametros sean correctos.
 *
 */
function AgregarSentencias() {
    var parametros = "";

    var valor = $("#select").val();
    if (valor === "Actualización") {
        var x = parseInt($("#x").val());
        var y = parseInt($("#y").val());
        var z = parseInt($("#z").val());
        var w = parseInt($("#w").val());
        if (x >= 1 && x <= parseInt($("#dimensionmatriz").val()) && y >= 1 && y <= parseInt($("#dimensionmatriz").val()) && z >= 1 && z <= parseInt($("#dimensionmatriz").val())) {
            $("#textArea").val($("#textArea").val() + "\n" + valor + " " + x + " " + y + " " + z + " " + w);
            parametros = (x - 1) + "-" + (y - 1) + "-" + (z - 1) + "-" + w;
            if (cantidadsentencias === 0) {
                CalcularMatriz("crear", parametros);
            } else {
                CalcularMatriz("calcular", parametros);
            }
            cantidadsentencias++;
        } else {
            Mensaje("error", "x,y,z deben se mayor o igual a 1 y menores que la dimension de la matriz");

        }
    } else {
        var x1 = parseInt($("#x1").val());
        var y1 = parseInt($("#y1").val());
        var z1 = parseInt($("#z1").val());
        var x2 = parseInt($("#x2").val());
        var y2 = parseInt($("#y2").val());
        var z2 = parseInt($("#z2").val());
        if (x1 >= 1 && x1 <= parseInt($("#dimensionmatriz").val()) && y1 >= 1 && y1 <= parseInt($("#dimensionmatriz").val()) && z1 >= 1 && z1 <= parseInt($("#dimensionmatriz").val())) {
            if (x2 >= 1 && x2 <= parseInt($("#dimensionmatriz").val()) && y2 >= 1 && y2 <= parseInt($("#dimensionmatriz").val()) && z2 >= 1 && z2 <= parseInt($("#dimensionmatriz").val())) {
                $("#textArea").val($("#textArea").val() + "\n" + valor + " " + x1 + " " + y1 + " " + z1 + " " + x2 + " " + y2 + " " + z2);
                parametros = (x1 - 1) + "-" + (y1 - 1) + "-" + (z1 - 1) + "-" + (x2 - 1) + "-" + (y2 - 1) + "-" + (z2 - 1);
                if (cantidadsentencias === 0) {
                    CalcularMatriz("crear", parametros);
                } else {
                    CalcularMatriz("calcular", parametros);
                }
                cantidadsentencias++;
            }
            else {
                Mensaje("error", "x2,y2,z2 deben se mayor o igual a 1 y menores que la dimension de la matriz");
            }
        } else {
            Mensaje("error", "x1,y1,z1 deben se mayor o igual a 1 y menores que la dimension de la matriz");

        }

    }
    VerificarLimites();
}

/**
 * Verifica si la cantidad de sentencias y la cantidad de casos ya se cumplieron
 *
 */
function VerificarLimites(){
    if (cantidadsentencias === parseInt($("#numerosentencias").val())) {
        if (cantidadcasos === parseInt($("#numerodecasos").val())) {
            Mensaje("exito", "ha terminado la configuración");
            InhabilitarTodo();
            ActivarModal();
        } else {
            $("#dimensionmatriz").removeAttr('disabled', 'disabled');
            $("#numerosentencias").removeAttr('disabled', 'disabled');
            $("#BtnAgregar").removeAttr('disabled', 'disabled');
            $("#select").attr('disabled', 'disabled');
            $("#BtnAgregarSentencias").attr('disabled', 'disabled');
            $("#sentencias").attr('disabled', 'disabled');

            cantidadsentencias = 0;
            Mensaje("exito", "configure el siguiente caso de prueba");
        }
    }
}
/**
 * inhabulita las opciones cuando se ha terminado el proceso
 *
 */
function InhabilitarTodo() {
    $("#dimensionmatriz").attr('disabled', 'disabled');
    $("#numerosentencias").attr('disabled', 'disabled');
    $("#BtnAgregar").attr('disabled', 'disabled');
    $("#select").attr('disabled', 'disabled');
    $("#BtnAgregarSentencias").attr('disabled', 'disabled');
    $("#sentencias").attr('disabled', 'disabled');
}

/**
 * inyecta las diferentes alarmas del proyecto segun su tipo, ya sea de exito o error
 *
 */
function Mensaje(tipo, mensaje) {
    if (tipo === "error") {
        $("#alertas").html("<div class='alert alert-danger' role='alert'><p>" + mensaje + "</p></div>");
    } else {
        $("#alertas").html("<div class='alert alert-success' role='alert'><p>" + mensaje + "</p></div>");
    }
}

/**
 * envia el el arreglo json al controlador
 * @param accion: ya sea de crear la matriz o simplemente ejecutar una sentencia
 *        parametros: los diferentes parametros de x,y,z,w ó x1,y1,z1,x2,y2,z2 segun pertenezcan
 */
function CalcularMatriz(accion, parametros) {
    var datos = {accion: accion, dimensionmatriz: $("#dimensionmatriz").val(), sentencia: $("#select").val(), cordenadas: parametros};
    $.ajax({
        method: "POST",
        url: "Controlador/CtlMatriz.php",
        data: datos,
        success: ProcesarCalcularmatriz,
        error: function () {
            Mensaje("error", "Error listando los inmuebles, intentelo de nuevo");
        }
    });
}

/**
 * Procesa la respuesta que trae desde el controlador
 * @param data: los datos qeu devulve el controlador
 */
var ProcesarCalcularmatriz = function (data) {
    if (data.split("+")[0] === "Actualizacion Completa") {
        Mensaje(data.split("+")[1], data.split("+")[0]);
    } else {
        respuesta = respuesta + " " + data.split("+")[0];  
        alert("respuesta:" + respuesta);
        
    }
};
/**
 * activa el modal donde se muestran los resultados
 */
function ActivarModal() {
    if (respuesta !== "") {
        $("#myModal").modal("show");
        $("#repuesta").html("<p>" + respuesta + "</p>");
    }
}
/**
 * reinicia el formulario
 */
function Recargarpg() {
    window.location.reload();
}
